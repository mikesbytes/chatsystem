//
// Created by user on 10/1/20.
//

#ifndef CHATSYSTEM_CLCOMMANDS_HPP
#define CHATSYSTEM_CLCOMMANDS_HPP

#include <string>
#include <optional>
#include <nlohmann/json.hpp>

namespace chsys {

nlohmann::json get_username();

void assign_username(const std::string &username);

void receive(const std::string &sender, const std::optional<std::string> &channel, const std::string &message);

}


#endif //CHATSYSTEM_CLCOMMANDS_HPP
