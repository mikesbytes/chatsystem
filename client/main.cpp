#include <threadpool.hpp>
#include <repeattaskrunner.hpp>
#include <network.hpp>
#include <rpcdispatcher.hpp>
#include <iostream>

int main() {
    // set up thread pool
    cyg::ThreadPool &tp = cyg::get_thread_pool("default");
    tp.spawn_workers(2);

    // init dispatcher
    cyg::RPCDispatcher dp;

    // set up network
    cyg::Network net;

    // new connection handler
    auto new_con_fn = [](cyg::SocketPtr ctx) {
        std::cout << "New connection: " << ctx->get_host() << ":" << ctx->get_port() << std::endl;
    };
    net.new_con_callback = new_con_fn;

    // disconnect handler
    auto disconnect_fn = [](cyg::SocketPtr ctx) {

    };
    net.disconnect_callback = disconnect_fn;

    // connect dispatcher
    auto dispatch_fn = [&dp](cyg::SocketPtr sock, const cyg::NetMessage &msg) {
        auto res = dp.parse_and_dispatch_message(cyg::from_net_message<std::string>(msg), sock);
        if (res.has_value()) {
            sock->get_network()->queue_send(sock, cyg::to_net_message(std::string(res->dump())));
        }
    };
    net.msg_callback = dispatch_fn;

    // network processing function for threadpool
    auto nproc_out = [&net]() {
        try {
            net.process_out();
        } catch (std::exception &e) {
            std::cout << "nproc_out exception: " << e.what() << std::endl;
        }
    };

    auto nproc_in = [&net]() {
        try {
            net.process_in();
        } catch (std::exception &e) {
            std::cout << "nproc_in exception: " << e.what() << std::endl;
        }
    };
    cyg::RepeatTaskRunner nproc_in_runner(nproc_in);
    nproc_in_runner.run(&tp);

    cyg::RepeatTaskRunner nproc_out_runner(nproc_out);
    nproc_out_runner.run(&tp);

    //connect
    auto sv_sock = net.connect_to("127.0.0.1", 3847);

    std::atomic<bool> running(true);
    while(running) {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }
}