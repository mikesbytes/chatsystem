//
// Created by user on 10/1/20.
//

#include <map>
#include "svcommands.hpp"

namespace chsys {

std::map<std::string, std::vector<cyg::SocketPtr>> channel_users;
std::map<std::string, cyg::SocketPtr> users;
std::vector<std::string> channels;

}
