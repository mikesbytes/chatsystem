//
// Created by user on 10/1/20.
//

#ifndef CHATSYSTEM_SVCOMMANDS_HPP
#define CHATSYSTEM_SVCOMMANDS_HPP

#include <string>
#include <vector>
#include <network.hpp>

namespace chsys {

void send(const std::string &recipient, const std::string &message);

std::vector<std::string> list_users();

std::vector<std::string> list_channels();

void channel_connect(cyg::SocketPtr client, std::string channel);

void channel_disconnect(cyg::SocketPtr client, std::string channel);

}


#endif //CHATSYSTEM_SVCOMMANDS_HPP
