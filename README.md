# COMP 3825 Networking project

## Chat system

Notes: The networking code will be lifted from [here](https://gitlab.com/mikesbytes/libcygnus) which is another project I wrote.

## Build instruction

Required dependencies: nlohmann json, fmtlib
These dependencies should be available in your system's repositories

```
git clone https://gitlab.com/mikesbytes/chatsystem.git --recurse-submodules
cd chatsystem
cmake -S . -B build
cd build
make -j8
```

This will output `chatsystem-sv` and `chatsystem-cl` binaries into the `build` directory

## Usage

Currently the only behavior is that you can start the server then start the client, and the server will print out
the host and port of the new connection.