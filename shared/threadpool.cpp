//
// Created by user on 6/13/19.
//

#include "threadpool.hpp"

#include <map>

namespace cyg {

std::map<std::string, ThreadPool> THREAD_POOLS;

ThreadPool &get_thread_pool(const std::string &key) {
    return THREAD_POOLS[key];
}

void ThreadPool::spawn_workers(int worker_count) {
    for (int i = 0; i < worker_count; ++i) {
        workers_.emplace_back(&ThreadPool::worker_, this);
    }
}

void ThreadPool::worker_() {
    while (!done_) {
        std::unique_ptr<ThreadTaskBase> task{nullptr};
        if (work_queue_.wait_dequeue_timed(task, std::chrono::milliseconds(200))) {
            task->execute();
        }
    }
}

void ThreadPool::destroy_() {
    done_ = true;
    for (auto &i : workers_) {
        if (i.joinable()) {
            i.join();
        }
    }
}

ThreadPool::ThreadPool() :
    done_(false)
{}

} // namespace cyg
