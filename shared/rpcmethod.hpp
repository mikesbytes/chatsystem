//
// Created by user on 3/9/20.
//

#ifndef LIBCYGNUS_RPCMETHOD_HPP
#define LIBCYGNUS_RPCMETHOD_HPP

#include <functional>
#include <optional>
#include <utility>

#include <nlohmann/json.hpp>
#include <fmt/format.h>
#include "isinstance.hpp"

template <typename T>
void to_json(nlohmann::json &j, const std::optional<T> &opt) {
    if (opt.has_value()) {
        j = opt.value();
    } else {
        j = nullptr;
    }
}

template <typename T>
void from_json(const nlohmann::json &j, std::optional<T> &opt) {
    if (j.is_null()) {
        opt = std::nullopt;
    } else {
        opt = j.get<T>();
    }
}

namespace cyg {

struct RPCInvalidParamsException : std::runtime_error
{
    using std::runtime_error::runtime_error;
};

template<typename T, typename... Args>
class RPCMethod {
            typedef nlohmann::json json;
            typedef std::tuple<typename std::decay<Args>::type...> ttype;
            typedef std::tuple<std::optional<typename std::decay<Args>::type>...> defaults_tup_type;
public:

    RPCMethod(
            std::function<T(Args...)> fn,
            std::array<std::string, sizeof...(Args)> param_names = {},
            defaults_tup_type param_defaults = defaults_tup_type()
    ) :
            param_names_(std::move(param_names)),
            function_(fn),
            defaults_(param_defaults)
    {

    }

    /*
    //dunno if I need this so it's staying for now
    RPCMethod(const RPCMethod<T, Args...> &wrapper) :
            param_names_(wrapper.param_names_),
            function_(wrapper.function_),
            defaults_(wrapper.defaults_)
    {

    }
     */

    T operator() (const json &params) {
        // check if arg is a single json object for custom handling
        if constexpr (sizeof...(Args) == 1 &&
                      std::is_same_v<std::tuple_element_t<0, ttype>, json>)
        {
            return function_(params);
        }

        ttype tup = unpack_params_(params);
        return std::apply(function_, tup);

    }

private:
    /// Unpack params from doc into tuple
    ttype unpack_params_(const json &params) {
        return unpack_params_(params, ttype());
    };

    template<size_t I = 0>
    ttype unpack_params_(const json &params, ttype unpacked) {
        if constexpr (I == sizeof...(Args)) {
            return std::move(unpacked);
        } else {
            std::get<I>(unpacked) = param_handler_(std::get<I>(defaults_), params, param_names_[I]);
            return std::move(unpack_params_<I + 1>(params, unpacked));
        }
    };

    /// handle individual parameters
    template <typename U>
    U param_handler_(std::optional<U> &fallback, const json &params, const std::string &param_name) {
        try {
            auto val = params.find(param_name);
            if (val == params.end()) {
                if constexpr (cyg::is_instance<U, std::optional>{}) { // missing optional param handler
                    return fallback.value_or(std::nullopt);
                } else {
                    if (fallback.has_value()) {
                        return fallback.value();
                    } else {
                        throw RPCInvalidParamsException(fmt::format("Missing required parameter: {}", param_name));
                    }
                }
            }
            try {
                if constexpr (std::is_same_v<U, nlohmann::basic_json<>>) {
                    return val.value();
                } else if constexpr (cyg::is_instance<U, std::optional>{}) { // optional param handling
                    return std::optional(val.value().get<typename U::value_type>());
                } else {
                    return val.value().get<U>();
                }
            } catch (const std::exception &e) {
                throw RPCInvalidParamsException(fmt::format("Invalid parameter: {}", param_name));
            }
        } catch (const json::type_error &e) {
            throw RPCInvalidParamsException("Invalid parameters object");
        }
    }

    std::function<T(Args...)> function_;
    std::array<std::string, sizeof...(Args)> param_names_;
    defaults_tup_type defaults_;
};

} // namespace cyg

#endif //LIBCYGNUS_RPCMETHOD_HPP
