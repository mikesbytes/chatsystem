//
// Created by user on 6/30/20.
//

#ifndef LIBCYGNUS_RPCCONVERSIONFUNCS_HPP
#define LIBCYGNUS_RPCCONVERSIONFUNCS_HPP

#include <string>
#include <nlohmann/json.hpp>

#include "network.hpp"

namespace cyg {

template<>
cyg::NetMessage to_net_message<std::string>(const std::string &str) {
    cyg::NetMessage msg;
    std::copy(str.begin(), str.end(), std::back_inserter(msg.data));
    return msg;
}

template<>
std::string from_net_message<std::string>(const NetMessage &msg) {
    return std::string(msg.data.begin(), msg.data.end());
}

template<>
cyg::NetMessage to_net_message<nlohmann::json>(const nlohmann::json &j) {
    cyg::NetMessage msg;
    std::string str = j.dump();
    std::copy(str.begin(), str.end(), std::back_inserter(msg.data));
    return msg;
}

template<>
nlohmann::json from_net_message<nlohmann::json>(const NetMessage &msg) {
    return nlohmann::json::parse(msg.data.begin(), msg.data.end());
}

}

#endif //LIBCYGNUS_RPCCONVERSIONFUNCS_HPP
