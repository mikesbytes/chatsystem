//
// Created by user on 3/18/20.
//

#include "rpcdispatcher.hpp"
#include "rpcconversionfuncs.hpp"


typedef nlohmann::json json;

namespace cyg {

std::optional<json> RPCDispatcher::dispatch_message(const json &j, SocketPtr sock) {
    json response = {{"jsonrpc","2.0"}};
    if (!j.contains("jsonrpc") ||
         j["jsonrpc"] != "2.0" ||
        !j.contains("method") ||
        !j["method"].is_string())
    {
        response["error"] = {{"code", -32600},
                             {"message", "Invalid RPC request"},
                             {"id", nullptr}};
        return response;
    }

    auto raw_method = raw_methods_.find(j["method"]);
    if (raw_method != raw_methods_.end()) {
        return raw_method->second(sock, j);
    }

    auto method = methods_.find(j["method"]);
    if (method == methods_.end()) {
        response["error"] = {{"code", -32601},
                             {"message", "RPC method not found"}};
        if (j.contains("id")) {
            response["error"]["id"] = j["id"];
        } else {
            response["error"]["id"] = nullptr;
        }
        return response;
    }

    json params = {};
    if (j.contains("params")) params = j["params"];

    method->second->load_response(response, params);

    if (!j.contains("id")) {
        return std::nullopt;
    }

    response["id"] = j["id"];

    return response;
}

std::optional<json> RPCDispatcher::parse_and_dispatch_message(const std::string &message, SocketPtr sock) {
    try {
        auto j = json::parse(message);
        return dispatch_message(j, sock);
    } catch (const json::parse_error &e) {
        json j = {{"jsonrpc","2.0"},
                  {"error",{
                      {"code", -32700},
                      {"message",e.what()}
                  },
                  {"id",nullptr}}};
        return j;
    }
}

void RPCDispatcher::bind_wrapper(const std::string & key, std::unique_ptr<RPCWrapperBase> method) {
    if (methods_.find(key) != methods_.end()) {
        // TODO: make this throw a more sensible error
        throw std::runtime_error(key + " is already bound");
    }

    methods_[key] = std::move(method);
}

void RPCDispatcher::process(SocketPtr sock, const NetMessage &msg) {
    auto res = parse_and_dispatch_message(cyg::from_net_message<std::string>(msg), sock);
    if (res.has_value()) {
        sock->queue_send(std::string(res->dump()));
    }
}

void RPCDispatcher::bind_raw(const std::string &key, std::function<std::optional<json>(SocketPtr, const json &)> fn) {
    if (raw_methods_.find(key) != raw_methods_.end()) {
        throw std::runtime_error(key + " is already bound");
    }

    raw_methods_[key] = std::move(fn);
}


}
