//
// Created by user on 3/22/20.
//

#include <sys/socket.h>
#include <netdb.h>
#include <stdexcept>
#include <fmt/format.h>
#include <arpa/inet.h>
#include <poll.h>
#include <unistd.h>

#include "network.hpp"

namespace cyg {

SocketPtr Network::connect_to(const std::string &host, uint16_t port, IP_TYPE ip_type) {
    struct addrinfo hints = {};
    struct addrinfo *sv_info;

    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    int status;
    if ((status = getaddrinfo(host.c_str(), std::to_string(port).c_str(), &hints, &sv_info)) != 0) {
        throw NetworkException(fmt::format("getaddrinfo error: {}", gai_strerror(status)));
    }

    // iterate over addresses if an ip type is preferred
    struct addrinfo *target = sv_info;
    if (ip_type != IP_TYPE::UNSPEC) {
        struct addrinfo *p;
        for (p = sv_info; p != NULL; p = p->ai_next) {
            if (ip_type == IP_TYPE::IPV4 && p->ai_family == AF_INET) {
                target = p;
            } else if (ip_type == IP_TYPE::IPV6 && p->ai_family == AF_INET6){
                target = p;
            }
        }
    }

    // get file descriptor
    int fd;
    if ((fd = socket(target->ai_family, target->ai_socktype, target->ai_protocol)) == -1) {
        freeaddrinfo(sv_info);
        throw(NetworkException(fmt::format("socket() error: {}", strerror(errno))));
    }

    // connect to the host
    if (connect(fd, target->ai_addr, target->ai_addrlen) == -1) {
        freeaddrinfo(sv_info);
        throw(NetworkException(fmt::format("connect() error: {}", strerror(errno))));
    }

    SocketPtr sock_p(new Socket);
    sock_p->fd_ = fd;
    sock_p->port_ = port;
    sock_p->host_ = host;
    sock_p->connected_.store(true);
    sock_p->network_ = this;

    freeaddrinfo(sv_info);

    add_pfd(sock_p->get_fd());
    {
        std::lock_guard guard(sockets_mutex_);
        sockets_[sock_p->get_fd()] = sock_p;
    }

    return std::move(sock_p);
}

ListenSocketPtr Network::listen(const std::string &host, uint16_t port, IP_TYPE ip_type) {
    struct addrinfo hints = {};
    struct addrinfo *sv_info;

    if (ip_type == IP_TYPE::IPV4) {
        hints.ai_family = AF_INET;
    } else if (ip_type == IP_TYPE::IPV6) {
        hints.ai_family = AF_INET6;
    } else {
        hints.ai_family = AF_UNSPEC;
    }
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    int status;
    if (host == "") {
        status = getaddrinfo(NULL, std::to_string(port).c_str(), &hints, &sv_info);
    } else {
        status = getaddrinfo(host.c_str(), std::to_string(port).c_str(), &hints, &sv_info);
    }
    if (status != 0) {
        throw NetworkException(fmt::format("getaddrinfo error: {}", gai_strerror(status)));
    }

    // get file descriptor
    int fd = socket(sv_info->ai_family, sv_info->ai_socktype, sv_info->ai_protocol);
    if (fd == -1) {
        freeaddrinfo(sv_info);
        throw(NetworkException(fmt::format("socket() error: {}", strerror(errno))));
    }

    // bind
    if (::bind(fd, sv_info->ai_addr, sv_info->ai_addrlen) == -1) {
        freeaddrinfo(sv_info);
        throw(NetworkException(fmt::format("bind() error: {}", strerror(errno))));
    }

    // listen
    if (::listen(fd, 10) == -1) {
        freeaddrinfo(sv_info);
        throw(NetworkException(fmt::format("listen() error: {}", strerror(errno))));
    }

    ListenSocketPtr sock_p(new ListenSocket);
    sock_p->fd_ = fd;
    sock_p->port_ = port;
    sock_p->host_ = host;
    sock_p->connected_.store(true);

    freeaddrinfo(sv_info);

    // add to poll fds
    add_pfd(sock_p->get_fd());
    {
        std::lock_guard guard(listen_sockets_mutex_);
        listen_sockets_[sock_p->get_fd()] = sock_p;
    }

    return sock_p;
}

Network::Network() :
    poll_fds_size_(5),
    poll_fds_used_(0)
{
    poll_fds_ = malloc(sizeof(struct pollfd) * 5);
}

Network::~Network() {
    free(poll_fds_);
}

void Network::add_pfd(int fd) {
    fds_mutex.lock();
    auto * pfds = static_cast<pollfd *>(poll_fds_);

    // realloc if needed
    if (poll_fds_used_ >= poll_fds_size_) {
        poll_fds_size_ *= 2; // double size
        poll_fds_ = realloc(pfds, sizeof(*pfds) * poll_fds_size_); // reallocate
    }

    pfds = static_cast<pollfd *>(poll_fds_);
    pfds[poll_fds_used_].fd = fd;
    pfds[poll_fds_used_].events = POLLIN;
    poll_fds_used_++;
    fds_mutex.unlock();
}

void Network::remove_pfd(int fd) {
    fds_mutex.lock();
    auto * pfds = static_cast<pollfd *>(poll_fds_);

    // find the pfd
    int i = 0;
    while (true) {
        if (i >= poll_fds_used_) throw NetworkException("remove_pfd(): Could not find fd for removal");
        if (pfds[i].fd == fd) break;
        ++i;
    }

    // swap the fd from the end to the indexed position
    pfds[i] = pfds[poll_fds_used_ - 1];

    // decrement the back
    --poll_fds_used_;

    fds_mutex.unlock();
}

void Network::process_in() {
    fds_mutex.lock_shared();

    // cast the void * to a pollfd * for convenience
    auto * pfds = (struct pollfd *)poll_fds_;

    int processed = 0;
    int poll_count = poll(pfds, poll_fds_used_, 500);

    for (uint32_t i = 0; i < poll_fds_used_; ++i) {
        if (processed >= poll_count) {
            fds_mutex.unlock_shared();
            return;
        } // nothing more to process

        // we got an event
        if (pfds[i].revents & POLLIN) {
            std::lock_guard guard_s(sockets_mutex_);
            // check regular sockets first since reads are more common than new connections
            auto it = sockets_.find(pfds[i].fd);
            if (it != sockets_.end()) {
                try {
                    auto msg = it->second->recv_msg();
                    if (msg.data.size() == 0) {
                        //disconnection
                        fds_mutex.unlock_shared();
                        remove_pfd(it->second->get_fd());
                        fds_mutex.lock_shared();
                        disconnect_callback(it->second);
                    } else {
                        msg_callback(it->second, msg);
                    }
                } catch (std::exception &e) {
                    fds_mutex.unlock_shared();
                    throw(e);
                }
            } else {
                // it must be a listen socket if it's not a regular socket
                try {
                    std::lock_guard guard(listen_sockets_mutex_);
                    //auto sock = listen_sockets_[pfds[i].fd]->accept();
                    //sock.network_ = this;
                    //SocketPtr new_socket = std::make_shared<Socket>();
                    SocketPtr new_socket = std::make_shared<Socket>(std::move(listen_sockets_[pfds[i].fd]->accept()));
                    new_socket->network_ = this;

                    // add the new socket to socket list and pfds
                    sockets_[new_socket->get_fd()] = new_socket;

                    fds_mutex.unlock_shared();
                    add_pfd(new_socket->get_fd());
                    fds_mutex.lock_shared();

                    new_con_callback(new_socket);
                } catch (std::exception &e) {
                    fds_mutex.unlock_shared();
                    throw(e);
                }
            }
            ++processed;
        }
    }
    fds_mutex.unlock_shared();
}

void Network::process_out() {
    std::pair<SocketPtr, NetMessage> to_send;
    if (out_queue_.wait_dequeue_timed(to_send, std::chrono::milliseconds(500))) {
        to_send.first->send(to_send.second);
    }

    //we have a message to send
}

void Network::queue_send(SocketPtr sock, const NetMessage &msg) {
    std::pair<SocketPtr, NetMessage> to_queue(sock, msg);
    out_queue_.enqueue(to_queue);
}

// SOCKET FUNCTIONS

Socket::Socket() :
    network_(nullptr),
    fd_(-1),
    host_(""),
    port_(0),
    connected_(false)
{

}

Socket::Socket(Socket &&socket) :
    network_(socket.network_),
    fd_(socket.fd_),
    host_(socket.host_),
    port_(socket.port_),
    connected_(socket.connected_.load())
{
    socket.fd_ = -1;
    socket.port_ = 0;
    socket.host_ = "";
    socket.network_ = nullptr;
    socket.connected_.store(false);
}

Socket::~Socket() {
    if (fd_ != -1) {
        close(fd_);
    }
}

void Socket::send(const NetMessage &msg) {
    if (!connected_.load()) return;
    uint32_t sent = 0;
    uint32_t size = htonl(msg.data.size());

    // send message length first
    while (sent < 4) {
        int res = ::send(fd_, (std::byte*)&size + sent, sizeof(size) - sent, 0);
        if (res == -1) {
            throw (NetworkException(fmt::format("send() message size error: {}", strerror(errno))));
        } else if(res == 0) {
            return;
        }
        sent += res;
    }
    sent = 0;

    // send the message
    while (sent < msg.data.size()) {
        int res = ::send(fd_, &msg.data[sent], msg.data.size() - sent, 0);
        if (res == -1) {
            throw(NetworkException(fmt::format("send() error: {}", strerror(errno))));
        }
        sent += res;
    }
}

void Socket::queue_send(const NetMessage &msg) {
    if (network_ == nullptr) throw NetworkException("queue_send(): network == nullptr");
    network_->queue_send(shared_from_this(), msg);
}

NetMessage Socket::recv_msg() {
    uint32_t received = 0;
    uint32_t size;
    NetMessage msg = {};
    if (!connected_.load()) return msg;

    // get message length first, check for disconnect
    while (received < 4) {
        int res = ::recv(fd_, (std::byte*)&size + received, sizeof(size) - received, 0);
        if (res == -1) {
            throw (NetworkException(fmt::format("message size recv() error: {}", strerror(errno))));
        } else if(res == 0) {
            connected_.store(false);
            return msg;
        }
        received += res;
    }
    received = 0;
    size = ntohl(size);

    msg.data.assign(size, 0);

    // send the message
    while (received < size) {
        int res = ::recv(fd_, &msg.data[received], size - received, 0);
        if (res == -1) {
            throw(NetworkException(fmt::format("recv() error: {}", strerror(errno))));
        } else if (res == 0) { // disconnect
            msg.data.clear();
            return msg;
        }
        received += res;
    }

    return msg;
}

Socket &Socket::operator=(Socket &&socket) {
    network_ = socket.network_;
    fd_ = socket.fd_;
    host_ = socket.host_;
    port_ = socket.port_;
    connected_.store(socket.connected_.load());
    socket.network_ = nullptr;
    socket.fd_ = -1;
    socket.host_ = "";
    socket.port_ = 0;
    return *this;
}



Socket ListenSocket::accept() {
    struct sockaddr_storage con_addr;
    socklen_t addr_len = sizeof(con_addr);

    int fd = ::accept(fd_, (struct sockaddr *)&con_addr, &addr_len);
    if (fd == -1) {
        throw(NetworkException(fmt::format("accept() error: {}", strerror(errno))));
    }

    char ip_str[INET6_ADDRSTRLEN];

    Socket sock;
    if (con_addr.ss_family == AF_INET) { // ipv4
        auto *addr = (struct sockaddr_in *)&con_addr;
        inet_ntop(AF_INET, &addr->sin_addr, ip_str, sizeof(ip_str));
        ntohs(addr->sin_port);
    } else { // ipv6
        auto *addr = (struct sockaddr_in6 *)&con_addr;
        inet_ntop(AF_INET6, &addr->sin6_addr, ip_str, sizeof(ip_str));
        ntohs(addr->sin6_port);
    }

    sock.host_ = std::string(ip_str);
    sock.fd_ = fd;
    sock.connected_.store(true);
    return sock;
}

} // namespace cyg